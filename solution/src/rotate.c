#include "rotate.h"

struct image rotate90(struct image const* image) {
    if (image->data) {
        struct image out_image = init(image->height, image->width);
        for (size_t i = 0; i < image->height; i++) {
            for (size_t j = 0; j < image->width; j++) {
                out_image.data[j * image->height + (image->height - 1 - i)] = image->data[i * image->width + j];
            }
        }
        return out_image;
    }
    else {
        return (struct image) {0};
    }
}

struct image rotate180(struct image const* image) {
    if (image->data) {
        struct image out_image = init(image->width, image->height);
        for (size_t i = 0; i < image->height; i++) {
            for (size_t j = 0; j < image->width; j++) {
                out_image.data[(image->width - 1 - j) + (image->height - 1 - i) * image->width] = image->data[i * image->width + j];
            }
        }
        return out_image;
    }
    else {
        return (struct image) {0};
    }
}
static struct image rotate270(struct image const* image){
    if (image->data) {
        struct image new_image = init(image->height, image->width);
        for (size_t i = 0; i < image->height; i++) {
            for (size_t j = 0; j < image->width; j++) {
                new_image.data[(image->width - 1 - j) * image->height + i] = image->data[j + i * image->width];
            }
        }
        return new_image;
    }
    else { return (struct image) {0};}
}



void rotate_image(struct image *img, int32_t angle){
    if (angle==0) return;
    angle = ((angle % 360) + 360) % 360;

    struct image new_image;
    switch (angle) {
        case 90:
            new_image = rotate90(img);
            break;
        case 180:
            new_image = rotate180(img);
            break;
        case 270:
            new_image = rotate270(img);
            break;
    }

    destructor(img);
    *img = new_image;
}
