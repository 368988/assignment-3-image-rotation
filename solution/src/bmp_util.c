#include "../include/bmp_util.h"


uint8_t get_padding(const uint64_t width) {
    return ((4 - (width * sizeof(struct pixel)) % 4) % 4);
}


enum read_status from_bmp( FILE* in, struct image* image ) {
    struct bmp_header bmp;

    if (!fread(&bmp, sizeof(struct bmp_header), 1, in)) { return READ_INVALID_HEADER; }

    if (bmp.biBitCount != 24) { return READ_INVALID_BITS; }

    if (bmp.bfType != 0x4d42) { return READ_INVALID_SIGNATURE; }

    *image = init(bmp.biWidth, bmp.biHeight);
    if (!image->data) { return READ_INVALID_MEMORY; }
    uint32_t padd = get_padding(bmp.biWidth);
    for (size_t i = 0; i < bmp.biHeight; i++) {
        if (fread(&image->data[((bmp.biHeight - i - 1) * bmp.biWidth)], sizeof(struct pixel), image->width, in) != image->width) {
            destructor(image);
            return READ_INVALID_BITS;
        }
        fseek(in, (long) padd, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* image) {
    uint32_t padd = get_padding(image->width);
    struct bmp_header bmpHeader;
    bmpHeader.bfReserved = 0;
    bmpHeader.biCompression = 0;
    bmpHeader.biPlanes = 1;
    bmpHeader.biClrUsed = 0;
    bmpHeader.biClrImportant = 0;
    bmpHeader.biXPelsPerMeter = 0;
    bmpHeader.biYPelsPerMeter = 0;
    bmpHeader.bOffBits = sizeof(struct bmp_header);
    bmpHeader.biSize = 40;
    bmpHeader.biBitCount = 24;
    bmpHeader.biSizeImage = image->height * (image->width * sizeof(struct pixel) + padd);
    bmpHeader.bfType = 0x4d42;
    bmpHeader.bfileSize = (uint32_t) (sizeof(struct bmp_header) + bmpHeader.biSizeImage);
    bmpHeader.biWidth = image->width;
    bmpHeader.biHeight = image->height;

    if (!fwrite(&bmpHeader, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < image->height; i++) {
        if (fwrite(&image->data[(image->height - i - 1) * image->width], sizeof(struct pixel), image->width, out) != image->width) {
            return WRITE_ERROR;
        }
        for (size_t j = 0; j < padd; j++) {
            fputc(0, out);
        }
    }
    return WRITE_OK;
}

