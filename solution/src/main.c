#include "../include/bmp_util.h"
#include "../include/rotate.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

int main( int argc, char** argv ) {
    FILE* f_in;
    FILE* f_out;
    struct image img;
    enum read_status read_status;
    enum write_status write_status;
    const char* angles[] = {"0", "90", "-90", "180", "-180", "270", "-270"};


    if (argc != 4) {
        fprintf(stderr, "Wrong number of arguments\n");
        return -1;
    }

    f_in = fopen(argv[1], "rb");
    if (!f_in) {
        fprintf(stderr, "error with input file");
        return -1;
    }

    read_status = from_bmp(f_in, &img);
    fclose(f_in);

    if (read_status != READ_OK) {
        fprintf(stderr, "error while reading file");
        return -1;
    }

    bool f = false;
    for (size_t i = 0; i < 7; i++) {
        if (strcmp(argv[3], angles[i]) == 0) {
            f = true;
        }
    }
    if (!f) {
        fprintf(stderr, "error with angle");
        destructor(&img);
        return -1;
    }

    rotate_image(&img, (int32_t)atoi(argv[3]));

    f_out = fopen(argv[2], "wb");
    if (!f_out) {
        fprintf(stderr, "error with output file");
        destructor(&img);
        return -1;
    }


    write_status = to_bmp(f_out, &img);
    if (write_status != WRITE_OK) {
        fprintf(stderr, "error with write");
        destructor(&img);
        return -1;
    }
    fclose(f_out);
    destructor(&img);

    return 0;
}
