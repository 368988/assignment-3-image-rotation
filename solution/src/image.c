#include "../include/image.h"

struct image init(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc((uint32_t)sizeof(struct pixel) * width * height)
    };
}

void destructor(struct image *image){
    if (image->data != NULL) {
        free(image->data);
        image->data = NULL;
    }
}

