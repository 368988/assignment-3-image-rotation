#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include <inttypes.h>
#include <malloc.h>
#include <stdio.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };


struct image {
    uint64_t width, height;
    struct pixel* data;
};


struct image init(uint64_t width, uint64_t height);

void destructor( struct image *image);


#endif //IMAGE_TRANSFORMER_IMAGE_H
