#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"

#include <stdio.h>

void rotate_image(struct image *img, int32_t angle);

#endif //IMAGE_TRANSFORMER_ROTATE_H
